const segitiga_param = function segitiga(n) {
    let last = [1], param = [last];
    for (let i = 0; i < n; i++) {
        const ls = [0].concat(last), rs = last.concat([0]);
        last = rs.map((r, i) => ls[i] + r);
        param = param.concat([last]);
    }
    return param;   
};

function segitiga_samasisi(param) {
    const last = param[param.length - 1];
    const nlen = `${last[last.length >> 1]}`.length;
    const str = (n) => {
        const sp = nlen - `${n}`.length;
        return `${" ".repeat(sp - (sp >> 1))}${n}${" ".repeat(sp >> 1)}`;
    };
    const space = " ".repeat(nlen);
    const pad = (i) => space.repeat(param.length - 1 - i);
    return param.map((l, i) => `${pad(i)}${l.map(str).join(space)}`).join("\n");
}

const n = 4;
console.log(segitiga_samasisi(segitiga_param(n)));