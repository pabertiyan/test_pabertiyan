'use strict';
module.exports = function(app) {
	var core = require('../controller/CoreController');
	// List Item
	app.route('/list-items')
		.get(core.listAllItem);
	// List Category
	app.route('/list-category')
		.get(core.listAllCategory);
	// List Cart
	app.route('/list-cart')
		.get(core.listAllCart);
	
	// Insert Item into Cart
	app.route('/insert-cart')
		.post(core.insertCart);
	
	// Delete Item into Cart
	app.route('/delete-cart/:id_item')
		.get(core.deleteCart)
		.delete(core.deleteCart);
		
	// List Cart
	app.route('/show-total')
		.get(core.showTotal);
		
	app.route('/insert-coupon/:couponCode')
		.get(core.insertCoupon)
		.put(core.insertCoupon);
};