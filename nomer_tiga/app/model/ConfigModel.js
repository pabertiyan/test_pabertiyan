'user strict';
var sql = require('./db.js');

var Task = function(task){
    this.id_item = task.id_item;
    this.qty_item = task.qty_item;
    this.price_item = task.price_item;
    this.total_price_item = task.total_price_item;
    this.coupon_code = task.coupon_code;
};
var coupon = function(task){
    this.coupon_code = task.coupon_code;
};
// Insert Item into table
Task.insertItem = function insertItem(newTask, result) {    
	sql.query("INSERT INTO item_product set ?", newTask, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});           
};
Task.insertCategory = function insertCategory(newTask, result) {    
	sql.query("INSERT INTO category_item set ?", newTask, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});           
};
Task.insertCart = function insertCart(newTask, result) {    
	sql.query("INSERT INTO cart_item set ?", newTask, function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(err, null);
		} else {
			console.log(res.insertId);
			result(null, res.insertId);
		}
	});           
};
Task.insertCoupon = function(id, result){
	sql.query("UPDATE cart_item set coupon_code = (SELECT id FROM coupon_item WHERE coupon_code = ?)", [id], function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {   
			result(null, res);
		}
	});            
};
// function for get all
Task.getAllItemList = function getAllItemList(result) {
	sql.query("SELECT * FROM item_product", function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log('tasks : ', res);  
			result(null, res);
		}
	});   
};
Task.getAllItemCat = function getAllItemCat(result) {
	sql.query("SELECT * FROM category_item", function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log('tasks : ', res);  
			result(null, res);
		}
	});   
};
Task.getAllItemCart = function getAllItemCart(result) {
	sql.query("SELECT * FROM cart_item", function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log('tasks : ', res);  
			result(null, res);
		}
	});   
};
// Show total price in cart
Task.showTotal = function showTotal(result) {
	sql.query("SELECT SUM(total_price_item) AS total FROM cart_item", function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			console.log('tasks : ', res); 
			result(null, res);
		}
	});   
};
// Delete Cart Item
Task.remove = function(id, result){
	sql.query("DELETE FROM cart_item WHERE id = ?", [id], function (err, res) {
		if(err) {
			console.log("error: ", err);
			result(null, err);
		} else {
			result(null, res);
		}
	}); 
};

module.exports= Task;