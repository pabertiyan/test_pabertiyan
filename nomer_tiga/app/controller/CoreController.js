'use strict';

var Task = require('../model/ConfigModel.js');
var coupon = require('../model/ConfigModel.js');
// Core Controller Get List
exports.listAllItem = function(req, res) {
	Task.getAllItemList(function(err, task) {
		console.log('controller')
		if (err)
			res.send(err);
			console.log('res', task);
		res.send(task);
	});
};
exports.listAllCategory = function(req, res) {
	Task.getAllItemCat(function(err, task) {
		console.log('controller')
		if (err)
			res.send(err);
			console.log('res', task);
		res.send(task);
	});
};
exports.listAllCart = function(req, res) {
	Task.getAllItemCart(function(err, task) {
		console.log('controller')
		if (err)
			res.send(err);
			console.log('res', task);
		res.send(task);
	});
};
exports.showTotal = function(req, res) {
	Task.showTotal(function(err, task) {
		console.log('controller')
		if (err)
			res.send(err);
			console.log('res', task);
		res.send(task);
	});
};
// Core Controller Insert Item
exports.insertCart = function(req, res) {
	var new_task = new Task(req.body);
	if(!new_task.id_item){
		//res.status(400).send({ error:true, message: 'Param is NULL! Please try again!' });
		res.status(400).send({ error:true, message: 'Param is NULL! Please try again!' });
	} else {
		Task.insertCart(new_task, function(err, task) {
			if (err)
				res.send(err);
			res.json(task);
		});
	}
};

exports.insertCoupon = function(req, res) {
	Task.insertCoupon(req.params.couponCode, function(err, task) {
		if (err)
		res.send(err);
		res.json(task);
	});
};

// Delete Item From Cart by ID
exports.deleteCart = function(req, res) {
	Task.remove( req.params.id_item, function(err, task) {
		if (err)
			res.send(err);
		res.json({ message: 'Item success deleted' });
	});
};