const 	express = require('express'),
		app = express(),
		bodyParser = require('body-parser');
		port = process.env.PORT || 27780;
const mysql = require('mysql');

const db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'nodejs'
});

db.connect();
app.listen(port);
console.log('This server running on port : ' + port);
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var routes = require('./app/routes/approutes');
routes(app);