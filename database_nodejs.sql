-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 19 Okt 2018 pada 00.57
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nodejs`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `cart_item`
--

CREATE TABLE `cart_item` (
  `id` int(11) NOT NULL,
  `id_item` int(11) NOT NULL,
  `qty_item` int(11) NOT NULL,
  `price_item` int(11) NOT NULL,
  `total_price_item` int(11) NOT NULL,
  `coupon_code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `cart_item`
--

INSERT INTO `cart_item` (`id`, `id_item`, `qty_item`, `price_item`, `total_price_item`, `coupon_code`) VALUES
(3, 1, 1, 150000, 150000, 1),
(4, 1, 1, 150000, 150000, 1),
(5, 1, 1, 150000, 150000, 1),
(6, 1, 1, 150000, 150000, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `category_item`
--

CREATE TABLE `category_item` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `category_item`
--

INSERT INTO `category_item` (`id`, `cat_name`) VALUES
(1, 'Fashion Wanita'),
(2, 'Fashion Pria'),
(3, 'Handphone & Tablet'),
(4, 'Laptop & Aksesoris'),
(5, 'Buku'),
(6, 'Software');

-- --------------------------------------------------------

--
-- Struktur dari tabel `coupon_item`
--

CREATE TABLE `coupon_item` (
  `id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `percent_coupon` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `coupon_item`
--

INSERT INTO `coupon_item` (`id`, `coupon_code`, `percent_coupon`) VALUES
(1, 'DISKONTEST', 10);

-- --------------------------------------------------------

--
-- Struktur dari tabel `item_product`
--

CREATE TABLE `item_product` (
  `id` int(11) NOT NULL,
  `nama_item` varchar(255) NOT NULL,
  `cat_item` int(11) NOT NULL,
  `height_item` int(11) NOT NULL,
  `price_item` int(11) NOT NULL,
  `disc_item` int(11) NOT NULL,
  `status_item` int(11) NOT NULL,
  `delete_item_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `item_product`
--

INSERT INTO `item_product` (`id`, `nama_item`, `cat_item`, `height_item`, `price_item`, `disc_item`, `status_item`, `delete_item_date`) VALUES
(1, 'Dress', 1, 100, 150000, 0, 1, '0000-00-00 00:00:00'),
(2, 'Rok', 1, 100, 50000, 0, 1, '0000-00-00 00:00:00'),
(3, 'Sweater', 2, 100, 150000, 0, 1, '0000-00-00 00:00:00'),
(4, 'Hoodie', 2, 100, 50000, 0, 1, '0000-00-00 00:00:00'),
(5, 'T-Shirt', 2, 100, 150000, 0, 1, '0000-00-00 00:00:00'),
(6, 'Polo Shirt', 2, 100, 50000, 0, 1, '0000-00-00 00:00:00'),
(7, 'Samsung A8 (2018)', 3, 200, 5600000, 0, 1, '0000-00-00 00:00:00'),
(8, 'Samsung A8+ (2018)', 3, 200, 6600000, 0, 1, '0000-00-00 00:00:00'),
(9, 'Samsung A8 Star', 3, 200, 7000000, 0, 1, '0000-00-00 00:00:00'),
(10, 'iPhone 7s', 3, 200, 8500000, 0, 1, '0000-00-00 00:00:00'),
(11, 'Asus ROG', 4, 1000, 15000000, 0, 1, '0000-00-00 00:00:00'),
(12, 'Asus X550ZE', 4, 1000, 7000000, 0, 1, '0000-00-00 00:00:00'),
(13, 'Dilan 1990', 5, 100, 75000, 0, 1, '0000-00-00 00:00:00'),
(14, 'Dilan 1991', 5, 100, 75000, 0, 1, '0000-00-00 00:00:00'),
(15, 'Dilan 1992', 5, 100, 75000, 0, 1, '0000-00-00 00:00:00'),
(16, 'Microsoft 365', 6, 100, 3600000, 0, 1, '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cart_item`
--
ALTER TABLE `cart_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category_item`
--
ALTER TABLE `category_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_item`
--
ALTER TABLE `coupon_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_product`
--
ALTER TABLE `item_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cart_item`
--
ALTER TABLE `cart_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `category_item`
--
ALTER TABLE `category_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `coupon_item`
--
ALTER TABLE `coupon_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `item_product`
--
ALTER TABLE `item_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
