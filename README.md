# test_pabertiyan

NodeJS Telkom
1. Jawabannya adalah angka ganjil 1,3,5,7,9
2. Segitiga Pasca ada di file nomor_dua.js
3. List Link (File ada di folder nomor_tiga)
	- list all item (http://127.0.0.1:27780/list-items) -> GET METHOD
	- list all category (http://127.0.0.1:27780/list-category) -> GET METHOD
	- list all cart (http://127.0.0.1:27780/list-cart) -> GET METHOD
	
	- insert item into cart (http://127.0.0.1:27780/insert-cart) -> POST METHOD -> 
		Param ({
			"id_item":"1",
			"qty_item":"1",
			"price_item":"150000",
			"total_price_item":"150000",
			"coupon_code":"0"
		})
	- delete item from cart (http://127.0.0.1:27780/delete-cart/1) -> DELETE METHOD / GET METHOD
	- insert discount coupon (http://127.0.0.1:27780/insert-coupon/DISKONTEST) -> GET METHOD / PUT METHOD
	- show total (http://127.0.0.1:27780/show-total) -> GET METHOD